import bpy
import bmesh

objects = bpy.data.objects
volumes_locations = {}

for ob in objects:
    data = ob.data
    bm = bmesh.new()
    bm.from_mesh(data)
    volume = bm.calc_volume()
    print("Object: " + ob.name + ", volume: " + str(volume) + ", location: " + str(ob.location))
    bm.free()

    found = False
    for entry in volumes_locations.items():
        if entry[1] == (volume, ob.location):
            found = True
            print(entry[0] + " has a duplicate: " + ob.name)
            # bpy.data.objects.remove(bpy.context.scene.objects[ob.name], do_unlink=True)
    if not found:
        volumes_locations[ob.name] = (volume, ob.location)
