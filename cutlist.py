import bpy
import os
import re
from collections import Counter

objects = bpy.context.scene.objects
for ob in objects:
    ob.select_set(ob.type == "MESH")

selection = bpy.context.selected_objects


class Item:
    def __init__(self, name, x, y, z, quantity):
        self.name = name
        self.x = x
        self.y = y
        self.z = z
        self.quantity = quantity

    def __hash__(self):
        return hash((self.name, self.x, self.y, self.z, self.quantity))

    def __eq__(self, other):
        return self.name == other.name

    def __str__(self):
        return 'Item(name=' + self.name + ', quantity=' + str(self.quantity) + ', x=' + str(self.x) + ', y=' + str(self.y) + ', z=' + str(self.z) + ')'


result = ""
pieces = []
sorted_pieces = []

for sel in selection:
    dims = sel.dimensions
    x = round(sel.dimensions.x * 100, 2)
    y = round(sel.dimensions.y * 100, 2)
    z = round(sel.dimensions.z * 100, 2)

    r = re.search("(.*?)\.\d+$", sel.name)
    name = sel.name.strip() if (r is None) else r.group(1).strip()
    piece = Item(name, x, y, z, 1)
    pieces.append(piece)

cnt = Counter()
for piece in pieces:
    cnt[piece] += 1

for k, v in dict(cnt).items():
    sorted_pieces.append(Item(k.name, k.x, k.y, k.z, v))


def sort_by_height(it):
    return it.z


sorted_pieces.sort(key=sort_by_height)

filename = os.path.join("/tmp", "cutlist.csv")
os.makedirs(os.path.dirname(filename), exist_ok=True)

file = open(filename, "w")
file.write("Name, Quantity, Length, Width, Height\n")
for item in sorted_pieces:
    file.write("%s, %d, %.02f, %.02f, %.02f\n" % (item.name, item.quantity, item.x, item.y, item.z))

file.write(result)
file.close()
